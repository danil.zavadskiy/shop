import type User from './User'
import type Address from './Address'
import type Product from './Product'

export default interface Order {
    id: number
    userInfo: User,
    products: Product[]
    shippingMethodId: number
    paymentMethodId: number
    addressInfo: Address
    notes: string
    price: number
}