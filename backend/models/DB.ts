import type Product from './Product'
import type Category from './Category'
import type Size from './Size'
import type ShippingMethod from './ShippingMethod'
import type PaymentMethod from './PaymentMethod'
import type Order from './Order'

export default interface DB {
    products: Product[],
    categories: Category[],
    sizes: Size[],
    shippingMethods: ShippingMethod[],
    paymentMethods: PaymentMethod[],
    orders: Order[]

}