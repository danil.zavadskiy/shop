export default interface Product {
    id: number
    image: string
    title: string
    discount: number
    description: string
    categoryId: number
    slug: string
}

