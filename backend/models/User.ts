export default interface User {
    firstName: string
    lastName: string
    phone: string
    email: string
}