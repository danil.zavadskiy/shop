export default interface Address {
    country: string
    city: string
    street: string
    apartment: string
}