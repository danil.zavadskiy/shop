import * as fs from 'fs'
import { faker } from '@faker-js/faker';
import type Category from './models/Category'
import type Product from './models/Product'
import type Size from './models/Size'
import type DB from './models/DB'
function idGenerator () {
    let counter = 1;
    return () => counter++
}
const nextProductId = idGenerator()
const nextSizeId = idGenerator()

const categories: Category[] = [
    {
        id: 1,
        title: 'Rings',
        slug: 'rings'
    },
    {
        id: 2,
        title: 'Earrings',
        slug: 'earrings'
    },
    {
        id: 3,
        title: 'Chains',
        slug: 'chains'
    }
]

function generateProductsByCategory(categoryId: number): Product[] {
    const products: Product[] = []
    let count = 0
    do {
        count = faker.datatype.number(100)
    } while (count === 0)

    for(let n = 0; n < count; n++) {
        products.push(generateProduct(categoryId))
    }
    return products
}

function generateProduct(categoryId: number): Product {
    const title = faker.commerce.productName()
    return {
        id: nextProductId(),
        image: faker.image.animals(630, 630, true),
        title,
        slug: title.toLowerCase().replace(/\s/ig, '-'),
        description: faker.commerce.productDescription(),
        discount: faker.datatype.number(60),
        categoryId,
    }
}


function generateSize(productId: number, title: string): Size {
    return {
        id: nextSizeId(),
        title,
        price: +faker.commerce.price(20, 500, 0),
        productId
    }
}

function generateSizes(productId: number): Size[] {
    const sizes: Size[] = []
    let count = 0
    do {
        count = faker.datatype.number(5)
    } while (count === 0)
    const sizesTitle = new Set<number>()
    do {
        sizesTitle.add(faker.datatype.number({ min: 10, max: 20 }))
    } while (sizesTitle.size !== count)
    Array.from(sizesTitle).sort((a, b) => a - b).forEach(title => {
        sizes.push(generateSize(productId, title.toString()))
    })
    return sizes
}

const db: DB = {
    categories,
    products: [],
    sizes: [],
    orders: [],
    shippingMethods: [
        {
            id: 1,
            title: "Free Shipping",
            price: 0
        },
        {
            id: 2,
            title: "Local",
            price: 10
        },
        {
            id: 3,
            title: "Flat rate",
            price: 20
        }
    ],
    paymentMethods: [
        {
            id: 1,
            title: "Direct bank transfer",
            description: "Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order will not be shipped until the funds have cleared in our account."
        },
        {
            id: 2,
            title: "Cash on delivery",
            description: "Pay with cash upon delivery."
        },
        {
            id: 3,
            title: "Paypal",
            description: "Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account."
        }
    ]
};

(() => {
    categories.forEach(({ id }) => {
        db.products.push(...generateProductsByCategory(id))
    })
    const productIds = db.products.map(p => p.id)
    productIds.forEach(productId => {
        db.sizes.push(...generateSizes(productId))
    })
    fs.writeFile('./backend/db.json', JSON.stringify(db), (err) => {
        // Checking for errors
        if (err) throw err;

        console.log("Done writing"); // Success
    })
})()