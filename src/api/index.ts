import ClientApi from '@/api/ClientApi'

export function useApi() {
    return new ClientApi()
}