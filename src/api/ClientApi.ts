import type Product from '@/models/Product'
import type Category from '@/models/Category'

import http from '@/api/http'
import type ShippingMethod from '@/models/ShippingMethod'
import type PaymentMethod from '@/models/PaymentMethod'

export interface ProductsWithPagination {
    pageCount: number
    data: Product[]
}

export default class ClientApi {

    async getHomeProducts(): Promise<Product[]> {
        const response = await http.get('/home-products')
        return response.data
    }

    async getCategories(): Promise<Category[]> {
        const response = await http.get('/categories')
        return response.data
    }

    async getProducts(page: number = 1):  Promise<ProductsWithPagination> {
        const response = await http.get('/products', {
            params: { page }
        })

        const regex = new RegExp(/page=([0-9]{1,2})>; rel="last"/gm)
        const pageCount = response.headers.link?.match(regex)?.join('')?.match(/[0-9]{1,2}/)?.shift() ?? 1
        return {
            pageCount: +pageCount,
            data: response.data
        }
    }

    async getShippingMethods(): Promise<ShippingMethod[]> {
        const response = await http.get('/shipping-methods')
        return response.data
    }

    async getPaymentMethods(): Promise<PaymentMethod[]> {
        const response = await http.get('/payment-methods')
        return response.data
    }
}