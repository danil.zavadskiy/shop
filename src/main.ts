import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import App from './App.vue'
import HomeLayout from '@/layouts/HomeLayout.vue'
import DefaultLayout from '@/layouts/DefaultLayout.vue'

import router from './router'

// Style Section
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import 'vue3-carousel/dist/carousel.css'
import './assets/css/vendor/font-awesome.css'
import './assets/css/vendor/flaticon/flaticon.css'
import './assets/css/vendor/slick.css'
import './assets/css/vendor/slick-theme.css'
import './assets/css/vendor/jquery-ui.min.css'
import './assets/css/vendor/sal.css'
import './assets/css/vendor/magnific-popup.css'
import './assets/css/vendor/base.css'
import './assets/scss/style.scss'

const app = createApp(App)
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
app.use(pinia)
app.use(router)

app.component('HomeLayout', HomeLayout)
app.component('DefaultLayout', DefaultLayout)

app.mount('#app')
