import { ref, reactive, computed } from 'vue'
import { defineStore } from 'pinia'
import type CartProduct from '@/models/CartProduct'

export const useCartStore = defineStore('cart', () => {
    const isOpen = ref(false)
    const items = reactive<CartProduct[]>([])

    const totalCartCount = computed(() => {
        return items.reduce((sum: number, item: CartProduct) => sum + item.count, 0)
    })

    const totalCartPrice = computed(() => {
        return items.reduce((sum: number, item: CartProduct) => {
            const price = item.selectedSize.price - item.selectedSize.price * (item.discount / 100)
            return sum + Math.round(price) * item.count
        }, 0)
    })

    function addToCart(product: CartProduct): void {
        const index = items.findIndex(item => item.selectedSize.id === product.selectedSize.id)
        if (index !== -1) {
            items.splice(index, 1, { ...items[index], count: items[index].count + 1 })
        } else {
            items.push(product)
        }
    }

    function removeFromCart(id: number): void {
        const index = items.findIndex(item => item.selectedSize.id === id)
        items.splice(index, 1)
    }

    function clearCart(): void {
        items.length = 0
    }

    function openCart(): void {
        isOpen.value = true
    }

    function closeCart(): void {
        isOpen.value = false
    }

    function incrementCount(id: number): void {
        const index = items.findIndex(item => item.selectedSize.id === id)
        items.splice(index, 1, { ...items[index], count: items[index].count + 1 })
    }

    function decrementCount(id: number): void {
        const index = items.findIndex(item => item.selectedSize.id === id)
        items.splice(index, 1, { ...items[index], count: items[index].count - 1 })
    }

    function setCount(id: number, count: number): void {
        const index = items.findIndex(item => item.selectedSize.id === id)
        items.splice(index, 1, { ...items[index], count })
    }

    return {
        isOpen,
        items,
        totalCartCount,
        totalCartPrice,
        openCart,
        closeCart,
        clearCart,
        addToCart,
        removeFromCart,
        incrementCount,
        decrementCount,
        setCount
    }
})