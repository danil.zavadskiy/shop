export default interface ShippingMethod {
    id: number
    title: string
    price: number
}