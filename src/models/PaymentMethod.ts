export default interface PaymentMethod {
    id: number
    title: string
    description: string
}