export default interface Size {
    id: number
    title: string
    price: number
    productId: number
}