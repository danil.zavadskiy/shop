import type Size from '@/models/Size'
import type Category from '@/models/Category'

export default interface Product {
    id: number
    image: string
    title: string
    description: string
    discount: number
    categoryId: number
    slug: string
    sizes: Size[]
    category: Category
}

