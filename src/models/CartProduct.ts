import type Product from '@/models/Product'
import type Size from '@/models/Size'

export default interface CartProduct extends Product {
    count: number
    selectedSize: Size
}