import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import CatalogView from '../views/CatalogView.vue'
import SingleProductView from '../views/SingleProductView.vue'
import ContactsView from '../views/ContactsView.vue'
import BlogView from '../views/BlogView.vue'
import SingleArticleView from '../views/SingleArticleView.vue'
import CartView from '../views/CartView.vue'
import CheckoutView from '../views/CheckoutView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/cart',
      name: 'cart',
      component: CartView,
      meta: {
        layout: 'HomeLayout',
      }
    },
    {
      path: '/checkout',
      name: 'checkout',
      component: CheckoutView,
      meta: {
        layout: 'HomeLayout',
      }
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: ContactsView
    },
    {
      path: '/blog',
      name: 'blog',
      component: BlogView
    },
    {
      path: '/blog/:slug',
      name: 'single-article',
      component: SingleArticleView
    },
    {
      path: '/catalog',
      name: 'catalog',
      component: CatalogView
    },
    {
      path: '/catalog/:category',
      name: 'category',
      component: CatalogView
    },
    {
      path: '/catalog/:category/:product',
      name: 'single-product',
      component: SingleProductView
    },
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        layout: 'HomeLayout',
      }
    }
  ]
})

export default router
