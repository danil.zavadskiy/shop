import type Product from '@/models/Product'

export const homeProductsPreview: Array<Product> = [
    {
        id: 1,
        image: '/src/assets/images/product/jewellery/product-11.png',
        title: 'Diamond Locket',
        price: 50,
        oldPrice: 60,
        discount: 15
    },
    {
        id: 2,
        image: '/src/assets/images/product/jewellery/product-12.png',
        title: 'Diamond Locket',
        price: 60,
        oldPrice: 60,
        discount: 0
    },
    {
        id: 3,
        image: '/src/assets/images/product/jewellery/product-13.png',
        title: 'Diamond Locket',
        price: 40,
        oldPrice: 60,
        discount: 20
    },
    {
        id: 4,
        image: '/src/assets/images/product/jewellery/product-14.png',
        title: 'Diamond Locket',
        price: 50,
        oldPrice: 50,
        discount: 0
    },
    {
        id: 5,
        image: '/src/assets/images/product/jewellery/product-15.png',
        title: 'Diamond Locket',
        price: 100,
        oldPrice: 100,
        discount: 0
    },
    {
        id: 6,
        image: '/src/assets/images/product/jewellery/product-1.png',
        title: 'Diamond Locket',
        price: 120,
        oldPrice: 60,
        discount: 50
    },
    {
        id: 7,
        image: '/src/assets/images/product/jewellery/product-2.png',
        title: 'Diamond Locket',
        price: 60,
        oldPrice: 60,
        discount: 0
    },
    {
        id: 8,
        image: '/src/assets/images/product/jewellery/product-3.png',
        title: 'Diamond Locket',
        price: 60,
        oldPrice: 60,
        discount: 0
    },
    {
        id: 9,
        image: '/src/assets/images/product/jewellery/product-4.png',
        title: 'Diamond Locket',
        price: 60,
        oldPrice: 60,
        discount: 0
    },
    {
        id: 10,
        image: '/src/assets/images/product/jewellery/product-5.png',
        title: 'Diamond Locket',
        price: 60,
        oldPrice: 60,
        discount: 0
    },
    {
        id: 11,
        image: '/src/assets/images/product/jewellery/product-6.png',
        title: 'Diamond Locket',
        price: 60,
        oldPrice: 60,
        discount: 0
    },
    {
        id: 12,
        image: '/src/assets/images/product/jewellery/product-7.png',
        title: 'Diamond Locket',
        price: 60,
        oldPrice: 60,
        discount: 0
    }
];